window.onload=init;



function init() {
    new Vue({
        el: "#app",
        data: {
            restaurants: [],
            name: '',
            cuisine: '',
            id:'',
            nbRestos:0,
            page :0,
            nbRestosParPage :5,
            nRecherche :'',
            nbPage:0,
            messageAffichage:'',


            name_modif:'',
            cuisine_modif:'',
            id_modif:'',
        },

        mounted(){
            console.log("Avant l'affichage html");
            this.getRestaurantsFromServer();
        },

        methods: {

            // utilisation de debounce pour ne pas envoye de requete a chaque modification
            // je vais l'utiliser soit pour rang ou la recherche par nom
            RechercheDebounce: _.debounce(
                function () {
                    this.getRestaurantsFromServer();
                }, 300),

            getRestaurantsFromServer(){
                let url = "http://localhost:8080/api/restaurants?page=" +
                            this.page + "&name=" + this.nRecherche +
                            "&pagesize=" + this.nbRestosParPage;
                console.log("Recherche des restaurants")
                fetch(url)
                    .then((reponseJSON) => {
                        return reponseJSON.json();
                    })
                    .then((reponseJS) => {
                        this.messageAffichage='';
                        this.restaurants = reponseJS.data;
                        this.nbRestos = reponseJS.count;
                        this.nbPage = Math.ceil(this.nbRestos / this.nbRestosParPage)-1
                    })
                    .catch(function (err) {
                        console.log("Une Erreur est produite " + err);
                });
            },

            supprimerRestaurant(index) {
                console.log("l'index à supprimer : " +index)
                this.envoieRequeteFetchDelete(index);
            },

            envoieRequeteFetchDelete(id) {
                let url = "http://localhost:8080/api/restaurants/" + id;
                fetch(url, {
                    method: "DELETE",
                })
                .then((responseJSON) => {
                    responseJSON.json()
                        .then((res) => { 
                            console.log("Restaurant supprimé");
                            this.getRestaurantsFromServer();
                            this.messageAffichage="Suppression Réussite";                            
                        });
                    })
                    .catch(function (err) {
                        console.log("Une Erreur est produite " +err);
                });
            },

            ajouterRestaurant(event) { 
                event.preventDefault();
                let form = event.target;
                let donneesFormulaire = new FormData(form);
               

                let url = "http://localhost:8080/api/restaurants/" ;

                fetch(url, {
                        method: "POST",
                        body: donneesFormulaire
                    })
                    .then((responseJSON) => {
                        responseJSON.json()
                            .then((res) => { // arrow function préserve le this
                                // Maintenant res est un vrai objet JavaScript
                                
                                console.log("Restaurant inséré");
                                
                                /* affichage message mais fixe
                                let div = document.querySelector("#AffichageMessage");
                                div.innerHTML = res.msg;
                                */

                                this.getRestaurantsFromServer();
                                this.messageAffichage=res.msg;

                                // remettre le formulaire à zéro
                                this.name = "";
                                this.cuisine = "";

                            });
                    })
                    .catch(function (err) {
                        console.log("Une Erreur est survenue " + err);
                    });
            },

            modifierRestaurant(r) {
                this.id_modif=r._id;
                this.name_modif=r.name;
                this.cuisine_modif = r.cuisine;
            },

            changerDonnéeResto(event){

                event.preventDefault();
                let form = event.target;
                let donneesFormulaire = new FormData(event.target);
                let id = this.id_modif;
                let url = "http://localhost:8080/api/restaurants/" + id;
                
                fetch(url, {
                    method: "PUT",
                    body: donneesFormulaire
                })

                .then((responseJSON) => {
                    responseJSON.json()
                        .then((res) => {
                            console.log(res.msg);
                            this.getRestaurantsFromServer();
                            this.messageAffichage="Restaurant modifié avec succes.";
                            
                            // videz le formulaire de modification
                            this.id_modif='';
                            this.name_modif='';
                            this.cuisine_modif='';
                            });
                    })
                    .catch(function (err) {
                        console.log("Une Erreur est produite " + err);
                });
                
            },

            getColor(index) {
                return (index % 2) ? 'white' : 'gray';
            },

            pagePrecedente(){
                this.page--;
                this.getRestaurantsFromServer();
            },

            pageSuivante(){
                this.page++;
                this.getRestaurantsFromServer();
            },

            pagePremiere(){
                this.page=0;
                this.getRestaurantsFromServer();
            },

            pageFin(){
                this.page=this.nbPage;
                this.getRestaurantsFromServer()
            }
        }
    })
}
